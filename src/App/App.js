import "../index.html";
import "mapbox-gl/dist/mapbox-gl.css";
import "../style.css";
import "./voletForm.js";
import "./inputDates";

import appConfig from "../../app.config";
import { LocalEvent } from "./LocalEvent";
import { ButtonRefresh } from "./ButtonRefresh";

const mapboxgl = require("mapbox-gl/dist/mapbox-gl.js");
class App {
	dayNames = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];
	monthNames = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];

	domNewEvTitle;
	domNewEvDesc;
	domNewEvStartAt;
	domNewEvEndAt;
	domNewEvLatitude;
	domNewEvLongitude;
	domBtnEvSubmit;

	localEventsTab = [];

	map;

	start() {
		console.log("Application started");

		this.initHTML();
		this.loadEvents();
		this.loadMap();
		this.renderEvents();

		console.log(this.localEventsTab);
	}

	loadMap() {
		// Clé API

		mapboxgl.accessToken = "pk.eyJ1IjoiaGFjaGVsIiwiYSI6ImNrbmZvczIzaDBnOW4ycW1lcDlxb2FlOXEifQ.JzMBLb68-F0svIPeqep-Yw";

		// Paramètres MapBox

		this.map = new mapboxgl.Map({
			container: "section-map",
			customAttribution: "TotoEvents",
			center: { lat: 42.69790540565282, lng: 2.813380049566234 },
			zoom: 10.5,
			minZoom: 10.5,
			maxZoom: 20,
			maxPitch: 60,
			maxBounds: [
				[2.18338, 42.395],
				[3.37981, 42.936]
			],
			style: "mapbox://styles/mapbox/dark-v10"
		});

		// Navigation MapBox

		const zoomCtrl = new mapboxgl.NavigationControl();
		this.map.addControl(zoomCtrl);

		// Récupération des coordonnées au click

		this.map.on("click", (evt) => {
			this.domNewEvLatitude.value = evt.lngLat.lat;
			this.domNewEvLongitude.value = evt.lngLat.lng;
		});

		// Bouton Refresh

		const refreshButton = new ButtonRefresh();
		this.map.addControl(refreshButton, "top-left");
		const btnReload = document.querySelector("#btn-reload-map");
		btnReload.addEventListener("click", this.onReloadClick.bind(this));
	}

	initHTML() {
		this.domNewEvTitle = document.querySelector("#input-event-title");

		this.domNewEvDesc = document.querySelector("#textarea-event-desc");

		this.domNewEvStartAt = document.querySelector("#input-event-start");
		this.domNewEvEndAt = document.querySelector("#input-event-end");

		this.domNewEvLongitude = document.querySelector("#input-event-lng");
		this.domNewEvLatitude = document.querySelector("#input-event-lat");

		this.domBtnEvSubmit = document.querySelector("#btn-event-submit");
		this.domBtnEvSubmit.addEventListener("click", this.onBtnNewAddClick.bind(this));

		this.domBtnDeleteAll = document.querySelector("#btn-event-deleteAll");
		this.domBtnDeleteAll.addEventListener("click", this.onDeleteAllClick.bind(this));

		// On enlève la classe error si présente

		const form = document.querySelector("form");
		form.addEventListener("click", this.onFormClick.bind(this));

		/* // Bouton reload de la map

		const btnReload = document.querySelector("#btn-reload-map");
		btnReload.addEventListener("click", this.onReloadClick); */
	}

	loadEvents() {
		this.localEventsTab = [];

		const storageContent = localStorage.getItem(appConfig.localStorageName);

		if (storageContent === null) {
			return;
		}

		let storedJson;

		try {
			storedJson = JSON.parse(storageContent);
		} catch (error) {
			localStorage.removeItem(appConfig.localStorageName);
			return;
		}

		for (let jsonLocalEvent of storedJson) {
			const lev = new LocalEvent(jsonLocalEvent);
			this.localEventsTab.push(lev);
		}
	}

	renderEvents() {
		const actualTime = Date.now(); // Date.parse(new Date()) c'est la même chose
		let varColor;
		const day = 24 * 60 * 60 * 1000; //en millisecondes

		// for (let localEvent of this.localEventsTab){
		for (let i = 0; i < this.localEventsTab.length; i++) {
			let startTime = Date.parse(this.localEventsTab[i].startAt);
			let endTime = Date.parse(this.localEventsTab[i].endAt);

			// Couleur en fonction du temps restant
			switch (true) {
				case startTime - 3 * day > actualTime:
					varColor = "#6aa84f";
					break;

				case startTime >= actualTime:
					varColor = "#e69138";
					break;

				case startTime < actualTime && actualTime < endTime:
					varColor = "#990000";
					break;

				case actualTime >= endTime:
					varColor = "grey";
					break;

				default:
					varColor = "#ccc";
					break;
			}

			// Si l'évènement est fini depuis + de 15 jours, on le supprime pour éviter la surcharge de données inutiles. Sinon, on l'affiche.

			if (endTime + 15 * day < actualTime) {
				this.localEventsTab.splice(i, 1);
				i = i--;
			} else {
				this.localEventsTab[i].marker = new mapboxgl.Marker({
					className: "marker",
					color: varColor
				});

				let textTitle = this.localEventsTab[i].title;
				let selectStartDate = this.localEventsTab[i].startAt.split(/[T|.]/i);
				let selectEndDate = this.localEventsTab[i].endAt.split(/[T|.]/i);
				let textStartDate = new Date(selectStartDate[0]).toLocaleDateString();
				let textEndDate = new Date(selectEndDate[0]).toLocaleDateString();

				this.localEventsTab[i].marker.getElement().title = `${textTitle} :: Du ${textStartDate} au ${textEndDate}`;

				this.localEventsTab[i].marker.setPopup(this.localEventsTab[i].popUp).setLngLat({ lng: this.localEventsTab[i].longitude, lat: this.localEventsTab[i].latitude });

				this.localEventsTab[i].marker.addTo(this.map);
			}
		}
	}

	onBtnNewAddClick() {
		// Etapes d'ajout
		// 1- On récupère et on vérifie les données saisie
		let hasError = false;
		const newTitle = this.domNewEvTitle.value.trim();
		if (newTitle === "") {
			this.domNewEvTitle.classList.add("error");
			this.domNewEvTitle.value = "";
			hasError = true;
		}

		const newStartAt = this.domNewEvStartAt.value;
		if (newStartAt === "") {
			this.domNewEvStartAt.classList.add("error");
			this.domNewEvStartAt.value = "";
			hasError = true;
		}

		const newEndAt = this.domNewEvEndAt.value;
		if (newEndAt === "") {
			this.domNewEvEndAt.classList.add("error");
			this.domNewEvEndAt.value = "";
			hasError = true;
		}

		let time1 = new Date(this.domNewEvStartAt.value);
		let time2 = new Date(this.domNewEvEndAt.value);
		let timeDiff = time2.getTime() - time1.getTime();

		if (timeDiff < 0) {
			this.domNewEvEndAt.classList.add("error");
			this.domNewEvEndAt.value = "";
			hasError = true;
		}

		const newLng = this.domNewEvLongitude.value.trim();
		if (newLng === "") {
			this.domNewEvLongitude.classList.add("error");
			this.domNewEvLongitude.value = "";
			hasError = true;
		}

		const newLat = this.domNewEvLatitude.value.trim();
		if (newLat === "") {
			this.domNewEvLatitude.classList.add("error");
			this.domNewEvLatitude.value = "";
			hasError = true;
		}

		// Si on a une ou plusieurs erreurs, on arrête le traitement

		if (hasError) {
			return;
		}

		// Si pas de description, on imprime "Pas de description"

		const newDesc = this.domNewEvDesc.value.trim();
		if (newDesc === "") {
			this.domNewEvDesc.value = "Pas de description";
		}

		// 3- Création d'une version JSON du LocalEvent, puis vidage des champs

		const newJson = {
			title: this.domNewEvTitle.value,
			description: this.domNewEvDesc.value,
			startAt: this.domNewEvStartAt.value,
			endAt: this.domNewEvEndAt.value,
			longitude: this.domNewEvLongitude.value,
			latitude: this.domNewEvLatitude.value
		};

		this.domNewEvTitle.value = this.domNewEvDesc.value = "";
		this.domNewEvStartAt.value = this.domNewEvEndAt.value = "";
		this.domNewEvLongitude.value = this.domNewEvLatitude.value = "";

		// 4- Fabriquer le LocalEvent
		const newLocalEvent = new LocalEvent(newJson);

		// 5- Reset des propriétés input
		this.domNewEvStartAt.setAttribute("max", "");
		this.domNewEvEndAt.setAttribute("min", "");

		// 6- Enregistrement

		// Enregistrement dans l'application (RAM)
		this.localEventsTab.push(newLocalEvent);

		// Enregistrement dans localStorage
		localStorage.setItem(appConfig.localStorageName, JSON.stringify(this.localEventsTab));

		this.loadEvents();
		this.renderEvents();
	}

	onFormClick(evt) {
		evt.target.classList.remove("error");
	}

	onReloadClick() {
		for (let i = 0; i < instance.localEventsTab.length; i++) {
			instance.localEventsTab[i].marker.remove(this.map);
		}

		instance.localEventsTab = [];

		instance.loadEvents();
		instance.renderEvents();
	}

	onDeleteAllClick() {
		localStorage.removeItem(appConfig.localStorageName);

		for (let i = 0; i < instance.localEventsTab.length; i++) {
			instance.localEventsTab[i].marker.remove(this.map);
		}

		this.localEventsTab = [];
		console.log(this.localEventsTab);
	}
}

const instance = new App();

export default instance;
