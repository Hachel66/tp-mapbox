// Control implemented as ES6 class

export class ButtonRefresh {
	onAdd(map) {
		this._map = map;
		this._container = document.createElement("button");
		this._container.innerHTML = "<i class='fas fa-sync-alt'></i>";
		this._container.className = "mapboxgl-ctrl";
		this._container.setAttribute("id", "btn-reload-map");
		this._container.setAttribute("type", "button");
		return this._container;
	}

	onRemove() {
		this._container.parentNode.removeChild(this._container);
		this._map = undefined;
	}
}
