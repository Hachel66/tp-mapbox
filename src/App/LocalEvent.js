const mapboxgl = require("mapbox-gl/dist/mapbox-gl.js");
import instance from "./App";
export class LocalEvent {
	title;
	description;
	startAt;
	endAt;
	longitude;
	latitude;

	marker;
	popUp;

	constructor(jsonLocalEvent) {
		this.title = jsonLocalEvent.title;
		this.description = jsonLocalEvent.description;
		this.startAt = jsonLocalEvent.startAt;
		this.endAt = jsonLocalEvent.endAt;
		this.longitude = jsonLocalEvent.longitude;
		this.latitude = jsonLocalEvent.latitude;

		this.popUp = new mapboxgl.Popup({
			className: "popup",
			setMaxWidth: "400px"
		});

		let dayStart = new Date(this.startAt).getDay();
		let dateStart = new Date(this.startAt).getDate();
		let monthStart = new Date(this.startAt).getMonth();

		let dayEnd = new Date(this.endAt).getDay();
		let dateEnd = new Date(this.endAt).getDate();
		let monthEnd = new Date(this.endAt).getMonth();

		this.popUp.setHTML(
			`<h3>${this.title}</h3>
            <h4>Description :</h4>
            <p>${this.description}</p>
            <h4>Dates de l'évènement :</h4>
            <p>Du ${instance.dayNames[dayStart]} ${dateStart} ${instance.monthNames[monthStart]} à ${this.timeText(this.startAt)}<br> 
            Au ${instance.dayNames[dayEnd]} ${dateEnd} ${instance.monthNames[monthEnd]} à ${this.timeText(this.endAt)}</p>
            <span>${this.dateBonus()}</span>`
		);

		/*  
            OLD VERSION     

            let selectStartDate = this.startAt.split(/[T|.]/i);
            let selectEndDate = this.endAt.split(/[T|.]/i);
            let textStartDate = new Date(selectStartDate[0]).toLocaleDateString();
            let textEndDate = new Date(selectEndDate[0]).toLocaleDateString();

            this.popUp.setHTML(

                `<h3>${this.title}</h3>
                <h4>Description :</h4>
                <p>${this.description}</p>
                <h4>Dates de l'évènement :</h4>
                <p>Du ${textStartDate} à ${selectStartDate[1]}<br> 
                Au ${textEndDate} à ${selectEndDate[1]}</p>
                <span>${this.dateBonus()}</span>` 

            ); 
        
        */
	}

	timeText(date) {
		let hours = new Date(date).getHours();
		let minutes = new Date(date).getMinutes();

		if (minutes < 10) {
			minutes = "0" + minutes;
		}

		date = [hours, minutes].join("h");
		return date;
	}

	dateBonus() {
		let ev = ``;

		const actualTime = Date.parse(new Date()); // Date.now() c'est la même chose

		let startTime = Date.parse(this.startAt);
		let endTime = Date.parse(this.endAt);

		const day = 24 * 60 * 60 * 1000; //en millisecondes
		const hour = 60 * 60 * 1000; //en millisecondes

		let remainingTime = startTime - actualTime;

		let dayText = Math.floor(remainingTime / day);
		let hourText = Math.floor((remainingTime % day) / hour);

		// Texte en fonction du temps restant

		let gramDay = "jour";
		let gramHour = "heure";

		dayText > 1 ? (gramDay += "s") : "";
		hourText > 1 ? (gramHour += "s") : "";

		switch (true) {
			case startTime - 3 * day > actualTime:
				return ev;

			case startTime >= actualTime:
				if (dayText == 0) {
					ev = `Attention, commence dans ${hourText} ${gramHour}`;
					return ev;
				} else {
					ev = `Attention, commence dans ${dayText} ${gramDay} et ${hourText} ${gramHour}`;
					return ev;
				}

			case startTime < actualTime && actualTime < endTime:
				ev = `L'évènement est actuellement en cours !`;
				return ev;

			case actualTime >= endTime:
				ev = `Quel dommage ! Vous avez raté cet évènement...`;
				return ev;
		}
	}

	toJSON() {
		return {
			title: this.title,
			description: this.description,
			startAt: this.startAt,
			endAt: this.endAt,
			longitude: this.longitude,
			latitude: this.latitude
		};
	}
}
