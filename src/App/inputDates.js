export default {}

document.addEventListener("DOMContentLoaded", function() {

    const inputStart = document.getElementById("input-event-start");
    const inputEnd = document.getElementById("input-event-end");

    inputStart.onchange = function () {
        inputEnd.setAttribute("min", this.value);
    }

    inputEnd.onchange = function () {
        inputStart.setAttribute("max", this.value);
    }

});


