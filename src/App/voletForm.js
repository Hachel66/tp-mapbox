export default {}

document.addEventListener("DOMContentLoaded", function() {

    const borderClick = document.querySelector("#form-border");
    const panneauForm = document.querySelector("#section-formulaire");

    borderClick.addEventListener("click", function() {

        panneauForm.classList.contains("formOpen") ? panneauForm.classList.remove("formOpen") : panneauForm.classList.add("formOpen") ;

    });

});

